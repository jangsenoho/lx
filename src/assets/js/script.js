//메뉴
$(document).ready(function(){
  $(".gnb > ul").mouseover(function(){
    $(".gnb > ul").parents(".header").addClass("on");
  });
  $(".gnb > ul").mouseleave(function(){
    $(".gnb > ul").parents(".header").removeClass("on");
  });
})






//datepicker
$("input[id^='datepicker']").each(function () {
  var _this = this.id;
  $('#' + _this).datepicker({
    regional: 'ko',
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    showOn: "button",
    buttonImage: "../assets/images/ico-calendar.png",
    buttonImageOnly: true,
  });
});

//드롭다운 매뉴
jQuery(document).ready(function (e) {
  function t(t) {
      e(t).bind("click", function (t) {
          t.preventDefault();
          e(this).parent().fadeOut()
      })
  }
  e(".dropdown-toggle").click(function () {
      var t = e(this).parents(".button-dropdown").children(".dropdown-menu").is(":hidden");
      e(".button-dropdown .dropdown-menu").hide();
      e(".button-dropdown .dropdown-toggle").removeClass("active");
      if (t) {
          e(this).parents(".button-dropdown").children(".dropdown-menu").toggle().parents(".button-dropdown").children(".dropdown-toggle").addClass("active")
      }
  });
  e(document).bind("click", function (t) {
      var n = e(t.target);
      if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-menu").hide();
  });
  e(document).bind("click", function (t) {
      var n = e(t.target);
      if (!n.parents().hasClass("button-dropdown")) e(".button-dropdown .dropdown-toggle").removeClass("active");
  })
});

//lnb fixed
var TopMenu, TopMenuPosition;
TopMenu = document.querySelector('.location');
TopMenuPosition = TopMenu.offsetTop; 
function submenu_bar_fixed(){
    if ( window.pageYOffset >= TopMenuPosition ) {
        TopMenu.classList.add("fixed");
    } else {
        TopMenu.classList.remove("fixed");
    }
}
document.addEventListener('scroll',submenu_bar_fixed);


//아코디언
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
//탭배너
$(document).ready(function(){
	
	$('.tab-banner .item').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('.tab-banner .item').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})
});